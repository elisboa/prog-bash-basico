# Aula 4 – Variáveis

- [Vídeo desta aula](https://youtu.be/5JZG9CfxQMw)

## 4.1 - Conceito

Uma variável é basicamente um **nome**, representado por uma **string** (basicamente, uma sequência de caracteres), que aponta para uma determinada informação, a que chamamos de **valor**. Com o shell, nós podemos criar, atribuir e eliminar variáveis.

Para criar uma variável, portanto, nós precisamos definir um nome e, em seguida, atribuir a ele um valor, o que é feito no shell através do operador de atribuição `=`.

Exemplos:

```
:~$ fruta="banana"
:~$ nota=10
:~$ nomes=("João" "Maria" "José")
:~$ retorno=$(whoami)
```

> Observe que não existem espaços antes ou depois do operador de atribuição!

Além dessa forma explícita de criar uma variável, nós também podemos atribuir valores a nomes através de comandos. É o caso, por exemplo, do comando interno `read`, que lê a informação que o usuário digita no terminal e a atribui a um nome...

```
:~$ read -p "Digite seu nome: " nome
Digite seu nome: Renata
:~$ echo $nome
Renata
:~$
```

## 4.2 - Nomeando variáveis

Por uma questão de boas práticas, os nomes das variáveis podem conter apenas **caracteres maiúsculos e minúsculos de A a Z** (sem acentos ou cedilha), **números** (nunca no começo!) e o **sublinhado** ( `_` ). Abaixo, podemos ver alguns exemplos de nomes válidos:

```
fruta  FRUTA    FRUTA1   Fruta1
_fruta _FRUTA   FRUTA_1  FRU_TA_1
```

Já esses nomes retornariam um erro no shell:

```
CAPÍTULO  2nome  nome!       RET*
1VAR      VAR 1  nome-aluno  ação
```

## 4.3 - Tipos de variáveis

Diferente de algumas outras linguagens, as variáveis não são declaradas especificando o tipo de valor que carregam. Ou seja, para criar uma variável do tipo **string**, basta criar um nome e atribuir a ele uma sequência de caracteres entre aspas duplas ou simples:

```
:~$ var1="Isso é uma string..."
:~$ var2='Isso também!'
```

Se você quiser criar uma variável numérica, basta atribuir um número ao nome...

```
:~$ valor1=10
:~$ valor2=23154
```

> Mesmo não sendo algo diretamente relacionado com variáveis, é bom saber desde já que **o bash não tem suporte nativo a operações matemáticas com ponto flutuante** (números com casas decimais), apenas com números inteiros. Para esse tipo de operação, nós temos que recorrer a recursos externos, como a linguagem de cálculo o **bc** ou o utilitário de calculadora **dc**.

## 4.4 - Variáveis vetoriais

Além das variáveis **escalares** (quando um nome aponta para apenas um valor) que nós vimos acima, nós também podemos criar variáveis **vetoriais** (quando um nome aponta para uma coleção de valores), as chamadas **arrays** (ou **matrizes**, em português).

Exemplos...

```
:~$ alunos=("João" "Maria" "Renata")
```

Ou assim...

```
:~$ alunos[0]="João"; alunos[1]="Maria"; alunos[2]="Renata"
```

Repare que o nome da variável é “**alunos**”, e ela contém três valores identificados pelos índices de `0` a `2`.

> Quando eu digo que não precisamos declarar variáveis no Bash, não estou dizendo que isso não seja possível! O ponto aqui é que, para o Bash, o conceito de **declaração** é um pouco diferente de outras linguagens. Para declarar (no sentido do Bash) o tipo da variável, nós utilizamos o comando interno **declare**.

Geralmente, porém, quando falamos de tipos de variáveis, no contexto do shell, nós estamos referindo a conceitos como:

| Conceito | Descrição  |
|---|---|
|**Variáveis locais**|Presentes na sessão atual do shell e disponíveis apenas para ele mesmo.|
|**Variáveis de ambiente**|Disponíveis para todos os processos filhos da sessão atual do shell.|
|**Variáveis do shell**|As variáveis especiais que o próprio shell define e que podem ser tanto locais quanto de ambiente.|

Dentro de um script, as variáveis ainda podem ser **globais** (quando estão disponíveis para todas as instruções do mesmo script) ou **locais** (quando só podem ser acessadas por algum bloco de código específico).

> Por padrão, todas as variáveis criadas dentro de um script são, para o próprio script, globais. Para torná-las locais, é preciso utilizar o comando interno `local`. Nós voltaremos a isso quando estivermos falando de **funções**.

## 4.5 - Variáveis inalteráveis (read-only)

Normalmente, nós podemos criar variáveis com um determinado valor e mudá-lo à vontade quando necessário...

```
:~$ fruta="banana"
:~$ echo $fruta
banana
:~$ fruta="laranja"
:~$ echo $fruta
laranja
```

Porém, existem casos onde precisamos garantir que um determinado valor não seja mudado. Com o comando interno `readonly`, o shell retornará um erro caso ocorra uma tentativa de alterar o valor da variável.

```
:~$ fruta="banana"
:~$ readonly fruta
:~$ fruta="laranja"
bash: fruta: a variável permite somente leitura
```

## 4.6 - Destruindo variáveis

Quando criamos uma variável, ela entra para uma lista e passa a ser rastreada pelo shell enquanto durar a sessão. Para remover a variável dessa lista, nós utilizamos o comando interno `unset`.

```
:~$ fruta="banana"
:~$ echo $fruta
banana
:~$ unset fruta
:~$ echo $fruta 

:~$
```

> Variáveis inalteráveis não podem ser destruídas! A única forma de deletar uma variável read-only é encerrando (ou reiniciando) a sessão do shell.

## 4.7 - Atribuindo saídas de comandos a variáveis

Também é possível atribuir a saída de um comando a uma variável utilizando uma expansão **substituições de comando**, que nós veremos em detalhes mais adiante no curso. Para isso, basta utilizar uma das sintaxes abaixo:

```
:~$ usuario=$(whoami)
:~$ echo $usuario
arthur
```

Ou então...

```
:~$ maquina=`hostname`
:~$ echo $maquina
excalibur
```

## 4.8 - Acessando os valores das variáveis

A essa altura, de tanto utilizarmos o comando `echo`, você já deve saber como acessar
o valor de uma variável. Mas vamos “formalizar” esse conhecimento: para ter acesso
ao valor armazenado numa variável, basta acrescentar o caractere `$` ao início de seu
nome, como neste exemplo:

```
:~$ read -p "Digite seu nome: " nome
Digite seu nome: Renata
:~$ mensagem="Olá, $nome!"
:~$ echo $mensagem
Olá, Renata!
```

No caso das variáveis vetoriais (**arrays**), porém, o procedimento é um pouco diferente, dependendo do que queremos ler da variável. Observe o exemplo:

```
:~$ nomes=("João" "Renata" "José")
:~$ echo $nomes
João
```

Dos três nomes dessa **array**, o shell só retornou o primeiro. Isso acontece porque, sem uma sintaxe especial, o shell irá tratar a variável `nomes` como se ela fosse uma **variável escalar**, e não uma **array**. Para acessar o conteúdo de uma array, nós utilizamos o seguinte formato:

```
${nome_da_variável[índice]}
```

> A rigor, a notação padrão para acesso (expansão) ao valor de uma variável é `${nome}`, mas as chaves são opcionais quando trabalhamos com variáveis escalares.

Aplicando essa ideia ao exemplo, para obter o valor de índice `1`, nós faríamos...

```
:~$ echo ${nomes[1]}
Renata
```

Isso porque os índices de uma array são contados a partir de zero.
